<?php

$to = 'info@web-resources.eu';

$subject = 'Test Email Template';

$headers = "From: Iron Web <info@ironmail.net>\r\n";
$headers .= "Reply-To: Merianos Nikos <info@web-resources.eu>\r\n";
$headers .= "CC: Merianos Vasilis <merianosnikos@gmail.com>\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message = file_get_contents( 'Kit_email KIBS_5.html' );

mail($to, $subject, $message, $headers);